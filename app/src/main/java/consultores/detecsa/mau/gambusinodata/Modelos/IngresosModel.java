
package consultores.detecsa.mau.gambusinodata.Modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IngresosModel {

    @SerializedName("IdIngreso")
    @Expose
    private String idIngreso;
    @SerializedName("Ingreso")
    @Expose
    private String ingreso;
    @SerializedName("Fecha")
    @Expose
    private String fecha;
    @SerializedName("Folio")
    @Expose
    private String folio;
    @SerializedName("IdImpuesto")
    @Expose
    private String idImpuesto;
    @SerializedName("IdCliente")
    @Expose
    private String idCliente;
    @SerializedName("Costo")
    @Expose
    private String costo;
    @SerializedName("IVA")
    @Expose
    private String iVA;
    @SerializedName("Total")
    @Expose
    private String total;
    @SerializedName("FechaCobro")
    @Expose
    private String fechaCobro;
    @SerializedName("ImporteCobro")
    @Expose
    private String importeCobro;
    @SerializedName("IdFormaPago")
    @Expose
    private String idFormaPago;
    @SerializedName("Comentarios")
    @Expose
    private String comentarios;
    @SerializedName("Cliente")
    @Expose
    private String cliente;
    @SerializedName("Impuesto")
    @Expose
    private String impuesto;
    @SerializedName("Tasa")
    @Expose
    private String tasa;
    @SerializedName("FormaPago")
    @Expose
    private String formaPago;

    public String getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(String idIngreso) {
        this.idIngreso = idIngreso;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getIVA() {
        return iVA;
    }

    public void setIVA(String iVA) {
        this.iVA = iVA;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(String fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public String getImporteCobro() {
        return importeCobro;
    }

    public void setImporteCobro(String importeCobro) {
        this.importeCobro = importeCobro;
    }

    public String getIdFormaPago() {
        return idFormaPago;
    }

    public void setIdFormaPago(String idFormaPago) {
        this.idFormaPago = idFormaPago;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

}
