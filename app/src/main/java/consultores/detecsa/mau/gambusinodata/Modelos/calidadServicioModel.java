package consultores.detecsa.mau.gambusinodata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class calidadServicioModel {

    @SerializedName("IdCalidadServicioConcepto")
    @Expose
    private String idCalidadServicioConcepto;
    @SerializedName("CalidadServicioConcepto")
    @Expose
    private String calidadServicioConcepto;
    @SerializedName("ano_mes")
    @Expose
    private String anoMes;
    @SerializedName("realizado")
    @Expose
    private String realizado;
    @SerializedName("meta")
    @Expose
    private String meta;
    @SerializedName("porc_accion")
    @Expose
    private String porcAccion;

    public String getIdCalidadServicioConcepto() {
        return idCalidadServicioConcepto;
    }

    public void setIdCalidadServicioConcepto(String idCalidadServicioConcepto) {
        this.idCalidadServicioConcepto = idCalidadServicioConcepto;
    }

    public String getCalidadServicioConcepto() {
        return calidadServicioConcepto;
    }

    public void setCalidadServicioConcepto(String calidadServicioConcepto) {
        this.calidadServicioConcepto = calidadServicioConcepto;
    }

    public String getAnoMes() {
        return anoMes;
    }

    public void setAnoMes(String anoMes) {
        this.anoMes = anoMes;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getPorcAccion() {
        return porcAccion;
    }

    public void setPorcAccion(String porcAccion) {
        this.porcAccion = porcAccion;
    }

}