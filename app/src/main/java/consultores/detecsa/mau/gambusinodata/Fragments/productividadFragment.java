package consultores.detecsa.mau.gambusinodata.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Adapters.EgresosAdapter;
import consultores.detecsa.mau.gambusinodata.Adapters.ProductividadAdapter;
import consultores.detecsa.mau.gambusinodata.Fragments.DetallesFragments.IngresoDetalleFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.Graficas.GraficasFragment;
import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Modelos.EgresosModel;
import consultores.detecsa.mau.gambusinodata.Modelos.ProductividadModel;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class productividadFragment extends Fragment {

    TextView tv;
    ImageView im;
    private static ConnectivityManager manager;
    RecyclerView recyclerView;
    ProductividadAdapter productividadAdapter;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_productividad, container, false);
        recyclerView=view.findViewById(R.id.rvProductividad);
        tv= view.findViewById(R.id.tvNetworkErrorProductividad);
        im=view.findViewById(R.id.imageViewNetworkProductividad);

        if (isOnline(getActivity())) {
            recyclerView.setVisibility(View.VISIBLE);
            //String baseURL= "http://gambusinodata.detecsa-consultores.com/";
            Datos d = new Datos(getContext());
            String baseURL= d.getDominio();
            Retrofit retrofit=new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();
            WebService webservice= retrofit.create(WebService.class);
            Call<List<ProductividadModel>> productividad=webservice.productividad();
            productividad.enqueue(new Callback<List<ProductividadModel>>() {
                @Override
                public void onResponse(Call<List<ProductividadModel>> call, Response<List<ProductividadModel>> response) {
                    if(response.body().size()<1){
                        recyclerView.setVisibility(View.INVISIBLE);
                        im.setVisibility(View.VISIBLE);
                        tv.setVisibility(View.VISIBLE);
                        im.setImageDrawable(getResources().getDrawable(R.drawable.ic_alert));
                        tv.setText("¡No hay elementos que mostrar!");
                    }else {
                        productividadAdapter = new ProductividadAdapter(response.body());
                        recyclerView.setAdapter(productividadAdapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                }

                @Override
                public void onFailure(Call<List<ProductividadModel>> call, Throwable t) {

                }
            });



            ///onclick de cada item

            final GestureDetector mGestureDetector = new GestureDetector(view.getContext(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean b) {

                }

                @Override
                public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                    try {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                        int position = recyclerView.getChildAdapterPosition(child);
                        String id = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvNumeroProductividad)).getText().toString();
                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                            Fragment detalles= new GraficasFragment();
                            Bundle bundle= new Bundle();
                            Datos d = new Datos(getContext());
                            String baseURL2= d.getDominio();
                            bundle.putString("url",baseURL2+"WebServiceApp/grafica_productividad/"+id);
                            detalles.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.content_nav_menu,detalles).commit();

                            return true;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                }
            });
        }else {
            im.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }

        return view;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

}
