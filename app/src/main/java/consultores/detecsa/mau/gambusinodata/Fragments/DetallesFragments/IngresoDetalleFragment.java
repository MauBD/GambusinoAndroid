package consultores.detecsa.mau.gambusinodata.Fragments.DetallesFragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Modelos.EgresosModel;
import consultores.detecsa.mau.gambusinodata.Modelos.IngresosModel;
import consultores.detecsa.mau.gambusinodata.R;


public class IngresoDetalleFragment extends Fragment {
    IngresosModel ingresosModel ;
    EgresosModel egresosModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

                ingresosModel= new IngresosModel();
                ingresosModel.setIdIngreso(getArguments().getString("id"));
                ingresosModel.setFolio(getArguments().getString("folio"));
                ingresosModel.setFecha(getArguments().getString("fecha"));
                ingresosModel.setIngreso(getArguments().getString("concepto"));
                ingresosModel.setCosto(getArguments().getString("costo"));
                ingresosModel.setIVA(getArguments().getString("iva"));
                ingresosModel.setTotal(getArguments().getString("total"));

                if(getArguments().getString("tipo")=="ingreso"){
                    ingresosModel.setCliente(getArguments().getString("cliente"));
                    ingresosModel.setFechaCobro(getArguments().getString("fechacobro"));
                }
                else if((getArguments().getString("tipo")=="egreso")){
                    ingresosModel.setCliente(getArguments().getString("proveedor"));
                    ingresosModel.setFechaCobro(getArguments().getString("fechapago"));
                }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_ingreso_detalle, container, false);
        //TextView tvId=view.findViewById(R.id.tvIdIngresoDetalle);
        TextView tvfolio=view.findViewById(R.id.tvFolioIngresoDetalle);
        TextView tvfecha=view.findViewById(R.id.tvFechaIngresoDetalle);
        TextView tvconcepto=view.findViewById(R.id.tvIngresoConceptoDetalle);
        TextView tvcliente=view.findViewById(R.id.tvClienteIngresoDetalle);
        TextView tvcosto=view.findViewById(R.id.tvCostoIngresoDetalle);
        TextView tviva=view.findViewById(R.id.tvIvaIngresoDetalle);
        TextView tvtotal=view.findViewById(R.id.tvTotalIngresoDetalle);
        TextView tvfechacobro=view.findViewById(R.id.tvFechaCobroIngresoDetalle);
        TextView TotalHeader=view.findViewById(R.id.tvCostoIngresoDetalleHeader);
        if(getArguments().getString("tipo")=="egreso"){
            TextView clienteProveedor= view.findViewById(R.id.tvTituloClienteProveedor);
            clienteProveedor.setText("Proveedor");
            TextView fechapago= view.findViewById(R.id.tvTituloFechaPagoCobro);
            fechapago.setText("Fecha de pago");
            TextView ingresoEgreso= view.findViewById(R.id.tvTituloIngresoEgreso);
            ingresoEgreso.setText("Egreso");
        }


        //tvId.setText(model.getIdIngreso());
        tvfolio.setText(ingresosModel.getFolio());
        tvfecha.setText(ingresosModel.getFecha());
        tvconcepto.setText(ingresosModel.getIngreso());
        tvcliente.setText(ingresosModel.getCliente());
        tvcosto.setText(ingresosModel.getCosto());
        tviva.setText(ingresosModel.getIVA());
        tvtotal.setText(ingresosModel.getTotal());
        tvfechacobro.setText(ingresosModel.getFechaCobro());
        TotalHeader.setText(ingresosModel.getTotal());

        return view;
    }


}
