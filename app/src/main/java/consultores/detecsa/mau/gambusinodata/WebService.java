package consultores.detecsa.mau.gambusinodata;

import java.util.List;
import consultores.detecsa.mau.gambusinodata.Modelos.EgresosModel;
import consultores.detecsa.mau.gambusinodata.Modelos.GastosModel;
import consultores.detecsa.mau.gambusinodata.Modelos.IngresosModel;
import consultores.detecsa.mau.gambusinodata.Modelos.ProductividadModel;
import consultores.detecsa.mau.gambusinodata.Modelos.Usuariomodel;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.FormUrlEncoded;
import retrofit2.Call;
import consultores.detecsa.mau.gambusinodata.Modelos.dominiosModel;
/**
 * Created by MAU on 27/01/2018.
 */

public interface WebService {
    @GET("getDom")
    Call<dominiosModel> getDom();

    @POST ("WebServiceApp/usuario")
    @FormUrlEncoded
    Call<Usuariomodel> login(@Field("user") String user, @Field("pwd") String pwd);

    @GET("WebServiceApp/ingresos")
    Call<List<IngresosModel>>ingresos();

    @GET("WebServiceApp/egresos")
    Call<List<EgresosModel>>egresos();

    @GET("WebServiceApp/gastos")
    Call<List<GastosModel>>gastos();

    @GET("WebServiceApp/productividad")
    Call<List<ProductividadModel>>productividad();

    @GET("WebServiceApp/acciones_venta")
    Call<List<consultores.detecsa.mau.gambusinodata.accionesVentaModel>>acciones_venta();

    @GET("WebServiceApp/calidadservicio")
    Call<List<consultores.detecsa.mau.gambusinodata.calidadServicioModel>>calidadservicio();

    @GET("WebServiceApp/accionescapacitacion")
    Call<List<consultores.detecsa.mau.gambusinodata.AccionesCapacitacionModel>>accionescapacitacion();

    @GET("WebServiceApp/preferenciasCliente")
    Call<List<consultores.detecsa.mau.gambusinodata.preferenciasClientesModel>>preferenciasCliente();
}
