package consultores.detecsa.mau.gambusinodata.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import consultores.detecsa.mau.gambusinodata.Modelos.dominiosModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConfiguracionActivity extends AppCompatActivity {

    Button btn ;
    EditText et;
    Datos d;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        d= new Datos(getApplicationContext());
        btn=(Button) findViewById(R.id.btnSetKey);
        et= (EditText) findViewById(R.id.etClaveEmpresa);
        tv=(TextView) findViewById(R.id.tvNombreEmpresa);
        setclave();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (validar()){
                    final String key=et.getText().toString();
                    final ProgressDialog progressDialog = new ProgressDialog(ConfiguracionActivity.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Buscando...");
                    progressDialog.show();
                    String url="http://gambusinodata.detecsa-consultores.com/DominiosRest/dominiosrest/getDom/"+key+"/";
                    Retrofit retrofit= new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
                    WebService webService= retrofit.create(WebService.class);
                    Call<dominiosModel> modelCall= webService.getDom();
                    modelCall.enqueue(new Callback<dominiosModel>() {
                        @Override
                        public void onResponse(Call<dominiosModel> call, Response<dominiosModel> response) {
                            dominiosModel model =response.body();
                            if(model.getDominioAPP()!=null){
                                int length=model.getDominioAPP().length()-1;
                                if (model.getDominioAPP().charAt(model.getDominioAPP().length()-1)!='/'){
                                    model.setDominioAPP("http://"+model.getDominioAPP()+"/");
                                }
                                d.setDominio(model.getDominioAPP());
                                d.setLogo(model.getLogotipo());
                                d.setClave(key);
                                d.setNombre(model.getNombre());
                                progressDialog.dismiss();
                                Snackbar.make(view, model.getDominioAPP(), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                                Intent intent= new Intent(ConfiguracionActivity.this,Login.class);
                                startActivity(intent);
                                finish();
                            }else {
                                progressDialog.dismiss();
                                Snackbar.make(view, "Clave invalida", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<dominiosModel> call, Throwable t) {
                            progressDialog.dismiss();
                            Snackbar.make(view, "Comprueba tu conexion o intentalo mas tarde", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    });
                }
            }
        });


    }


    boolean validar(){
        boolean valid = true;

        String key = et.getText().toString();


        if (key.isEmpty()) {
            et.setError("Ingresa una clave");
            valid = false;
        } else {
            et.setError(null);
        }

        return valid;
    }

    void setclave(){
        if(d.getNombre()!= null){
            tv.setVisibility(View.VISIBLE);
            tv.setText(d.getNombre());
        }
        et.setText(d.getClave());
    }
}
