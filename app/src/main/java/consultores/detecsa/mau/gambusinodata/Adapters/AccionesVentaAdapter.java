package consultores.detecsa.mau.gambusinodata.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.accionesVentaModel;

/**
 * Created by Desarrollo3 on 19/02/2018.
 */

public class AccionesVentaAdapter extends RecyclerView.Adapter<AccionesVentaAdapter.ViewHolderAccionesVenta>{

    List<accionesVentaModel> lista;

    public AccionesVentaAdapter(List<accionesVentaModel> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolderAccionesVenta onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.accionesventalayout,null,false);
        return new ViewHolderAccionesVenta(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderAccionesVenta holder, int position) {
        holder.llenardatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderAccionesVenta extends RecyclerView.ViewHolder {
        TextView accion,anomes,meta,realizado,porcentaje;
        public ViewHolderAccionesVenta(View itemView) {
            super(itemView);

            accion= (TextView) itemView.findViewById(R.id.tvAccionRvAccionVenta);
            anomes=(TextView) itemView.findViewById(R.id.tvAñoFechaRvAccionVenta);
            meta=(TextView) itemView.findViewById(R.id.tvMetaRvAccionVenta);
            realizado=itemView.findViewById(R.id.tvRealizadoRvAccionVenta);
            porcentaje=(TextView) itemView.findViewById(R.id.tvPorcentajeRvAccionVenta);
        }


        public void llenardatos(accionesVentaModel model) {
            accion.setText(model.getAccionVentaConcepto());
            anomes.setText(model.getAnoMes());
            meta.setText(model.getMeta());
            realizado.setText(model.getRealizado());
            Float Realizado=(Float.parseFloat(model.getPorcAccion())*100);
            porcentaje.setText(Realizado.toString());
        }
    }
}
