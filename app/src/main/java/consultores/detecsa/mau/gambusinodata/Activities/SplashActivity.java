package consultores.detecsa.mau.gambusinodata.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Modelos.Usuariomodel;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends Activity {

    ImageView iv;
    TextView tv;
    RelativeLayout rv;
    String usrr,pwd;
    Usuariomodel usr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Datos datos=new Datos(getApplicationContext());
        rv=findViewById(R.id.activity_splash);
        iv=(ImageView) findViewById(R.id.ivErrorSplash);
        tv=(TextView) findViewById(R.id.tvErrorSplash);
        usrr=datos.getUsr();
        pwd=datos.getPwd();
        Log.e("datos: ",usrr+","+pwd);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (usrr!=null && pwd!=null){
                    //String baseURL= "http://gambusinodata.detecsa-consultores.com/";
                    String baseURL= datos.getDominio();
                    Retrofit retrofit=new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();

                    WebService webservice= retrofit.create(WebService.class);
                    retrofit2.Call<Usuariomodel> usuario=webservice.login(usrr,pwd);
                    usuario.enqueue(new Callback<Usuariomodel>() {
                        @Override
                        public void onResponse(retrofit2.Call<Usuariomodel> call, Response<Usuariomodel> response) {
                            if(response.isSuccessful()){
                                usr= response.body();
                                if(usr.getSuccess()){
                                    Intent intent = new Intent(SplashActivity.this,NavMenu.class);
                                    intent.putExtra("user",usr.getNombreusuario());
                                    intent.putExtra("mail",usr.getCorreo());
                                    //progressDialog.dismiss();
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Intent intent = new Intent(SplashActivity.this, Login.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }else {
                                iv.setVisibility(View.VISIBLE);
                                tv.setVisibility(View.VISIBLE);
                                onclick();
                                /*Intent intent = new Intent(SplashActivity.this, Login.class);
                                startActivity(intent);
                                finish();*/
                            }
                        }
                        @Override
                        public void onFailure(retrofit2.Call<Usuariomodel> call, Throwable t) {
                            iv.setVisibility(View.VISIBLE);
                            tv.setVisibility(View.VISIBLE);
                            onclick();
                            /*Intent intent  = new Intent(SplashActivity.this, Login.class);
                            startActivity(intent);
                            finish();*/
                        }
                    });
                }else {
                    Intent intent  = new Intent(SplashActivity.this, Login.class);
                    startActivity(intent);
                    finish();
                }
            }
        },1000);
    }

    void onclick(){
        rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}
