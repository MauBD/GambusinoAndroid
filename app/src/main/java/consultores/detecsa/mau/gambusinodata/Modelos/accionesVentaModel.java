package consultores.detecsa.mau.gambusinodata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class accionesVentaModel {

    @SerializedName("IdAccionVentaConcepto")
    @Expose
    private String idAccionVentaConcepto;
    @SerializedName("AccionVentaConcepto")
    @Expose
    private String accionVentaConcepto;
    @SerializedName("ano_mes")
    @Expose
    private String anoMes;
    @SerializedName("realizado")
    @Expose
    private String realizado;
    @SerializedName("meta")
    @Expose
    private String meta;
    @SerializedName("porc_accion")
    @Expose
    private String porcAccion;

    public String getIdAccionVentaConcepto() {
        return idAccionVentaConcepto;
    }

    public void setIdAccionVentaConcepto(String idAccionVentaConcepto) {
        this.idAccionVentaConcepto = idAccionVentaConcepto;
    }

    public String getAccionVentaConcepto() {
        return accionVentaConcepto;
    }

    public void setAccionVentaConcepto(String accionVentaConcepto) {
        this.accionVentaConcepto = accionVentaConcepto;
    }

    public String getAnoMes() {
        return anoMes;
    }

    public void setAnoMes(String anoMes) {
        this.anoMes = anoMes;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getPorcAccion() {
        return porcAccion;
    }

    public void setPorcAccion(String porcAccion) {
        this.porcAccion = porcAccion;
    }

}
