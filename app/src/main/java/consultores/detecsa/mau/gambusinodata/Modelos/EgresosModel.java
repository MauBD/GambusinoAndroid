package consultores.detecsa.mau.gambusinodata.Modelos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EgresosModel {

    @SerializedName("IdEgreso")
    @Expose
    private String idEgreso;
    @SerializedName("Egreso")
    @Expose
    private String egreso;
    @SerializedName("Fecha")
    @Expose
    private String fecha;
    @SerializedName("Folio")
    @Expose
    private String folio;
    @SerializedName("IdImpuesto")
    @Expose
    private String idImpuesto;
    @SerializedName("IdProveedor")
    @Expose
    private String idProveedor;
    @SerializedName("Costo")
    @Expose
    private String costo;
    @SerializedName("IVA")
    @Expose
    private String iVA;
    @SerializedName("Total")
    @Expose
    private String total;
    @SerializedName("FechaPago")
    @Expose
    private String fechaPago;
    @SerializedName("ImportePago")
    @Expose
    private String importePago;
    @SerializedName("IdFormaPago")
    @Expose
    private String idFormaPago;
    @SerializedName("Comentarios")
    @Expose
    private String comentarios;
    @SerializedName("Proveedor")
    @Expose
    private String proveedor;
    @SerializedName("Impuesto")
    @Expose
    private String impuesto;
    @SerializedName("Tasa")
    @Expose
    private String tasa;
    @SerializedName("FormaPago")
    @Expose
    private String formaPago;

    public String getIdEgreso() {
        return idEgreso;
    }

    public void setIdEgreso(String idEgreso) {
        this.idEgreso = idEgreso;
    }

    public String getEgreso() {
        return egreso;
    }

    public void setEgreso(String egreso) {
        this.egreso = egreso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getIVA() {
        return iVA;
    }

    public void setIVA(String iVA) {
        this.iVA = iVA;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getImportePago() {
        return importePago;
    }

    public void setImportePago(String importePago) {
        this.importePago = importePago;
    }

    public String getIdFormaPago() {
        return idFormaPago;
    }

    public void setIdFormaPago(String idFormaPago) {
        this.idFormaPago = idFormaPago;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

}