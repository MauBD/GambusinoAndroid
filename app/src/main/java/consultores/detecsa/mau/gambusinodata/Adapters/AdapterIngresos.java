package consultores.detecsa.mau.gambusinodata.Adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Fragments.DetallesFragments.IngresoDetalleFragment;
import consultores.detecsa.mau.gambusinodata.Modelos.IngresosModel;
import consultores.detecsa.mau.gambusinodata.R;

/**
 * Created by MAU on 29/01/2018.
 */

public class AdapterIngresos extends RecyclerView.Adapter<AdapterIngresos.ViewHolderIngresos> {

    List<IngresosModel> listaIngresos;

    public AdapterIngresos(List<IngresosModel> listaIngresos) {
        this.listaIngresos = listaIngresos;
    }

    @Override
    public ViewHolderIngresos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.ingresolayout,null,false);
        return new ViewHolderIngresos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderIngresos holder, int position) {
        holder.llenarDatos(listaIngresos.get(position));
    }

    @Override
    public int getItemCount() {
        return listaIngresos.size();
    }

    public class ViewHolderIngresos extends RecyclerView.ViewHolder  {
       TextView id,ingreso,fecha,folio,cliente,costo,iva,total,fechacobro;

        public ViewHolderIngresos(View itemView) {
            super(itemView);
            id=itemView.findViewById(R.id.tvIdIngresoRV);
            ingreso=itemView.findViewById(R.id.tvIngresoConceptoRv);
            fecha=itemView.findViewById(R.id.tvFechaIngresoRV);
            cliente=itemView.findViewById(R.id.tvClienteIngresoRv);
            total=itemView.findViewById(R.id.tvTotalIngresoRv);
            costo=itemView.findViewById(R.id.tvCostoIngresoRv);
            iva=itemView.findViewById(R.id.tvIvaIngresoRv);
            fechacobro=itemView.findViewById(R.id.tvFechaCobroIngresoRv);
            folio=itemView.findViewById(R.id.tvFolioIngresoRv);



        }

        public void llenarDatos(IngresosModel ingresosModel) {
            id.setText(ingresosModel.getIdIngreso());
            ingreso.setText(ingresosModel.getIngreso());
            fecha.setText(ingresosModel.getFecha());
            cliente.setText(ingresosModel.getCliente());
            total.setText("$"+ingresosModel.getTotal());
            folio.setText(ingresosModel.getFolio());
            costo.setText(ingresosModel.getCosto());
            iva.setText(ingresosModel.getIVA());
            fechacobro.setText(ingresosModel.getFechaCobro());
        }

    }
}
