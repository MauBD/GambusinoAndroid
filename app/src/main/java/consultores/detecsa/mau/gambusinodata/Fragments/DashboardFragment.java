package consultores.detecsa.mau.gambusinodata.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.R;


public class DashboardFragment extends Fragment {
    TextView tv;
    ImageView im;
    private static ConnectivityManager manager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        View layout=inflater.inflate(R.layout.fragment_dashboard,container,false);
        layout.setPadding(0,0,0,0);
        WebView wv=layout.findViewById(R.id.wvdashboard);
        tv= layout.findViewById(R.id.tvNetworkErrorDash);
        im=layout.findViewById(R.id.imageViewNetworkDash);
        wv.setPadding(0,0,0,0);
        //String url=getResources().getString(R.string.baseurl)+"WebServiceApp/dashboard";
        Datos d = new Datos(getContext());
        String url= d.getDominio()+"WebServiceApp/dashboard";
        if (isOnline(getActivity())) {
            wv.setVisibility(View.VISIBLE);
            wv.loadUrl(url);
        } else {
            im.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }
        //wv.loadUrl("http://gambusinodata.detecsa-consultores.com/WebServiceApp/dashboard");
        return layout;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}
