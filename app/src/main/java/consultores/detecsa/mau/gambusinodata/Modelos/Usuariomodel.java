        package consultores.detecsa.mau.gambusinodata.Modelos;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Usuariomodel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("mensaje")
    @Expose
    private String mensaje;
    @SerializedName("IdUsuario")
    @Expose
    private String idUsuario;
    @SerializedName("Usuario")
    @Expose
    private String usuario;
    @SerializedName("IdPerfil")
    @Expose
    private String idPerfil;
    @SerializedName("nombreusuario")
    @Expose
    private String nombreusuario;
    @SerializedName("Correo")
    @Expose
    private String correo;
    @SerializedName("clave_aut")
    @Expose
    private String claveAut;
    @SerializedName("SesionMaxima")
    @Expose
    private String sesionMaxima;
    @SerializedName("SesionAbierta")
    @Expose
    private String sesionAbierta;
    @SerializedName("AccesoApp")
    @Expose
    private String accesoApp;
    @SerializedName("imagen_usuario")
    @Expose
    private String imagenUsuario;
    @SerializedName("Comentarios")
    @Expose
    private String comentarios;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("IdUsuarioCreo")
    @Expose
    private String idUsuarioCreo;
    @SerializedName("FechaCreacion")
    @Expose
    private String fechaCreacion;
    @SerializedName("IdUsuarioModifico")
    @Expose
    private String idUsuarioModifico;
    @SerializedName("FechaModifico")
    @Expose
    private String fechaModifico;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClaveAut() {
        return claveAut;
    }

    public void setClaveAut(String claveAut) {
        this.claveAut = claveAut;
    }

    public String getSesionMaxima() {
        return sesionMaxima;
    }

    public void setSesionMaxima(String sesionMaxima) {
        this.sesionMaxima = sesionMaxima;
    }

    public String getSesionAbierta() {
        return sesionAbierta;
    }

    public void setSesionAbierta(String sesionAbierta) {
        this.sesionAbierta = sesionAbierta;
    }

    public String getAccesoApp() {
        return accesoApp;
    }

    public void setAccesoApp(String accesoApp) {
        this.accesoApp = accesoApp;
    }

    public String getImagenUsuario() {
        return imagenUsuario;
    }

    public void setImagenUsuario(String imagenUsuario) {
        this.imagenUsuario = imagenUsuario;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdUsuarioCreo() {
        return idUsuarioCreo;
    }

    public void setIdUsuarioCreo(String idUsuarioCreo) {
        this.idUsuarioCreo = idUsuarioCreo;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdUsuarioModifico() {
        return idUsuarioModifico;
    }

    public void setIdUsuarioModifico(String idUsuarioModifico) {
        this.idUsuarioModifico = idUsuarioModifico;
    }

    public String getFechaModifico() {
        return fechaModifico;
    }

    public void setFechaModifico(String fechaModifico) {
        this.fechaModifico = fechaModifico;
    }

}