package consultores.detecsa.mau.gambusinodata.Modelos;
import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by Desarrollo3 on 04/09/2017.
 Clase para guardar el dominio y la clave de la escuela
 Fichero "3v41c3zC.dat" guarda la clave de la escuela
 fichero "d0m.dat" guarda el dominio de la base de dicha escuela
 */

public class Datos {
    private Context context;
    public Datos(Context context){
        this.context=context;
    }


    public String getDominio(){
        return Leer("d0m.dat");
    }
    public boolean setDominio(String dom){
        return Escribir("d0m.dat",dom);
    }

    public String getClave(){
        return Leer("3v41c3zC.dat");
    }
    public boolean setClave(String clv){
        return Escribir("3v41c3zC.dat",clv);
    }

    public String getLogo(){return Leer("gol.dat");}
    public boolean setLogo(String url){return Escribir("gol.dat",url);}

    public String getNombre(){return Leer("nam.dat");}
    public boolean setNombre(String name){return Escribir("nam.dat",name);}

    public String getUsr(){return Leer("rsuonisubmag.dat");}
    public boolean setUsr(String usr){return Escribir("rsuonisubmag.dat",usr);}

    public String getPwd(){return Leer("dwpg4m8u51n0.dat");}
    public boolean setPwd(String dwp){return Escribir("dwpg4m8u51n0.dat",dwp);}


    private Boolean Escribir(String File,String data){
        boolean success;
        try{

            FileOutputStream fos= context.getApplicationContext().openFileOutput(File,context.getApplicationContext().MODE_PRIVATE);

            OutputStreamWriter osw = new OutputStreamWriter(fos);

            // Escribimos el String en el archivo
            osw.write(data);
            osw.flush();
            osw.close();
            success=true;

        }catch (IOException ex){
            ex.printStackTrace();
            success=false;
        }

        return  success;
    }

    private String Leer(String File){
        String data =null;

        try{
            FileInputStream fis = context.getApplicationContext().openFileInput(File);
            InputStreamReader isr = new InputStreamReader(fis);

            char[] inputBuffer = new char[100];
            String s = "";

            int charRead;
            while((charRead = isr.read(inputBuffer)) > 0){
                // Convertimos los char a String
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                s += readString;

                inputBuffer = new char[100];
            }

            if(s.length()!=0){
                data=s;
            }

            isr.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return data;

    }
}