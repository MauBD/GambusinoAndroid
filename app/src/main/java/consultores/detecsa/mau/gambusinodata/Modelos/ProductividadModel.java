package consultores.detecsa.mau.gambusinodata.Modelos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductividadModel {

    @SerializedName("IdEmpleado")
    @Expose
    private String idEmpleado;
    @SerializedName("Empleado")
    @Expose
    private String empleado;
    @SerializedName("NoEmpleado")
    @Expose
    private String noEmpleado;
    @SerializedName("IdTipoProductividad")
    @Expose
    private String idTipoProductividad;
    @SerializedName("Nomina")
    @Expose
    private String nomina;
    @SerializedName("IdArea")
    @Expose
    private String idArea;
    @SerializedName("Correo")
    @Expose
    private String correo;
    @SerializedName("Comentarios")
    @Expose
    private String comentarios;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("IdDepartamento")
    @Expose
    private String idDepartamento;
    @SerializedName("Departamento")
    @Expose
    private String departamento;
    @SerializedName("TipoProductividad")
    @Expose
    private String tipoProductividad;
    @SerializedName("Horas")
    @Expose
    private String horas;

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getNoEmpleado() {
        return noEmpleado;
    }

    public void setNoEmpleado(String noEmpleado) {
        this.noEmpleado = noEmpleado;
    }

    public String getIdTipoProductividad() {
        return idTipoProductividad;
    }

    public void setIdTipoProductividad(String idTipoProductividad) {
        this.idTipoProductividad = idTipoProductividad;
    }

    public String getNomina() {
        return nomina;
    }

    public void setNomina(String nomina) {
        this.nomina = nomina;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getTipoProductividad() {
        return tipoProductividad;
    }

    public void setTipoProductividad(String tipoProductividad) {
        this.tipoProductividad = tipoProductividad;
    }

    public String getHoras() {
        return horas;
    }

    public void setHoras(String horas) {
        this.horas = horas;
    }

}