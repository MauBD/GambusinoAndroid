package consultores.detecsa.mau.gambusinodata.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.calidadServicioModel;

/**
 * Created by Desarrollo3 on 20/02/2018.
 */

public class CalidadAdapter extends RecyclerView.Adapter<CalidadAdapter.ViewHolderCalidad> {

    List<calidadServicioModel> lista;

    public CalidadAdapter(List<calidadServicioModel> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolderCalidad onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.accionesventalayout,null,false);

        return new ViewHolderCalidad(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderCalidad holder, int position) {
           holder.llenardatos( lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderCalidad extends RecyclerView.ViewHolder {

        TextView Titulo, anofecha,meta, realizado,porcentaje;
        public ViewHolderCalidad(View itemView) {
            super(itemView);
            //ocupamos los mismos views que acciones venta
            Titulo=itemView.findViewById(R.id.tvAccionRvAccionVenta);
            anofecha= itemView.findViewById(R.id.tvAñoFechaRvAccionVenta);
            meta=itemView.findViewById(R.id.tvMetaRvAccionVenta);
            realizado=itemView.findViewById(R.id.tvRealizadoRvAccionVenta);
            porcentaje=itemView.findViewById(R.id.tvPorcentajeRvAccionVenta);
        }

        public void llenardatos(calidadServicioModel model) {
            Titulo.setText(model.getCalidadServicioConcepto());
            anofecha.setText(model.getAnoMes());
            meta.setText(model.getMeta());
            realizado.setText(model.getRealizado());
            Float real;
            if(model.getPorcAccion()=="" || model.getPorcAccion()==null){
                real=Float.parseFloat("0");
            }else {
                real= Float.parseFloat(model.getPorcAccion());
            }
            porcentaje.setText(real.toString());
        }
    }
}
