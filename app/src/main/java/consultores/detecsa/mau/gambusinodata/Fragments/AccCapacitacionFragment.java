package consultores.detecsa.mau.gambusinodata.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Adapters.AccCapacitacionAdapter;
import consultores.detecsa.mau.gambusinodata.AccionesCapacitacionModel;
import java.util.List;

import consultores.detecsa.mau.gambusinodata.Adapters.AccionesVentaAdapter;
import consultores.detecsa.mau.gambusinodata.Fragments.Graficas.GraficasFragment;
import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AccCapacitacionFragment extends Fragment {

    TextView tv;
    ImageView im;
    private static ConnectivityManager manager;
    RecyclerView rv;
    AccCapacitacionAdapter adapter;
    public AccCapacitacionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final View view=inflater.inflate(R.layout.fragment_acc_capacitacion, container, false);
        rv=view.findViewById(R.id.rvCapacitacion);
        tv= view.findViewById(R.id.tvNetworkErrorCap);
        im=view.findViewById(R.id.imageViewNetworkCap);

        if (isOnline(getActivity())) {
            rv.setVisibility(View.VISIBLE);
            //String url= getResources().getString(R.string.baseurl);
            Datos d = new Datos(getContext());
            String url= d.getDominio();
            Retrofit retrofit= new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
            WebService webService= retrofit.create(WebService.class);

            Call<List<AccionesCapacitacionModel>> capacitacion= webService.accionescapacitacion();

            capacitacion.enqueue(new Callback<List<AccionesCapacitacionModel>>() {
                @Override
                public void onResponse(Call<List<AccionesCapacitacionModel>> call, Response<List<AccionesCapacitacionModel>> response) {
                    //
                    if(response.body().size()<1){
                        rv.setVisibility(View.INVISIBLE);
                        im.setVisibility(View.VISIBLE);
                        tv.setVisibility(View.VISIBLE);
                        im.setImageDrawable(getResources().getDrawable(R.drawable.ic_alert));
                        tv.setText("¡No hay elementos que mostrar!");
                    }else {
                        adapter= new AccCapacitacionAdapter(response.body());
                        rv.setAdapter(adapter);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                    //
                    /*adapter= new AccCapacitacionAdapter(response.body());
                    rv.setAdapter(adapter);
                    rv.setLayoutManager(new LinearLayoutManager(getActivity()));*/
                }

                @Override
                public void onFailure(Call<List<AccionesCapacitacionModel>> call, Throwable t) {

                }
            });

            ///onclick de cada item

            final GestureDetector mGestureDetector = new GestureDetector(view.getContext(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });

            rv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean b) {

                }

                @Override
                public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                    try {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                        int position = recyclerView.getChildAdapterPosition(child);
                        String titulo = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvAccionRvAccionVenta)).getText().toString();
                        String realizado=((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvPorcentajeRvAccionVenta)).getText().toString();
                        Float Realizado=Float.parseFloat(realizado);
                        Float norealizado=100-Realizado;

                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                            Fragment detalles= new GraficasFragment();
                            Bundle bundle= new Bundle();
                            Datos d = new Datos(getContext());
                            String url2= d.getDominio();
                            bundle.putString("url",url2+"WebServiceApp/grafica_capacitacion/"+titulo+"/"+Realizado+"/"+norealizado);
                            detalles.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.content_nav_menu,detalles).commit();

                            return true;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                }
            });


        }else {
            im.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }


        return view;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}
