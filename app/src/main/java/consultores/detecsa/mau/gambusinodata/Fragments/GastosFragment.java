package consultores.detecsa.mau.gambusinodata.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Adapters.GastosFijosAdapter;
import consultores.detecsa.mau.gambusinodata.Fragments.Graficas.GraficasFragment;
import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Modelos.GastosModel;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GastosFragment extends Fragment {

    TextView tv;
    ImageView im;
    private static ConnectivityManager manager;
    RecyclerView rv;
    GastosFijosAdapter adapter;
    FloatingActionButton verGrafica;
    public GastosFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        View  view= inflater.inflate(R.layout.fragment_gastos, container, false);
        rv=view.findViewById(R.id.rvGastosFijos);
        verGrafica=view.findViewById(R.id.floatingChartGastos);
        tv= view.findViewById(R.id.tvNetworkErrorGastos);
        im=view.findViewById(R.id.imageViewNetworkGastos);

        if (isOnline(getActivity())) {
            rv.setVisibility(View.VISIBLE);
            verGrafica.setVisibility(View.VISIBLE);
            //String baseUrl="http://gambusinodata.detecsa-consultores.com/";
            Datos d = new Datos(getContext());
            String baseUrl= d.getDominio();
            Retrofit retrofit= new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
            WebService webService= retrofit.create(WebService.class);
            Call<List<GastosModel>> gastos= webService.gastos();
            gastos.enqueue(new Callback<List<GastosModel>>() {
                @Override
                public void onResponse(Call<List<GastosModel>> call, Response<List<GastosModel>> response) {
                    if(response.body().size()<1){
                        rv.setVisibility(View.INVISIBLE);
                        im.setVisibility(View.VISIBLE);
                        tv.setVisibility(View.VISIBLE);
                        im.setImageDrawable(getResources().getDrawable(R.drawable.ic_alert));
                        tv.setText("¡No hay elementos que mostrar!");
                    }else {
                        adapter = new GastosFijosAdapter(response.body());
                        rv.setAdapter(adapter);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                }

                @Override
                public void onFailure(Call<List<GastosModel>> call, Throwable t) {

                }
            });


            verGrafica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment grafica=new GraficasFragment();
                    Bundle bundle = new Bundle();
                    Datos d = new Datos(getContext());
                    String baseURL= d.getDominio();
                    bundle.putString("url",baseURL+"webServiceApp/grafica_gastosfijos");
                    grafica.setArguments(bundle);

                    getFragmentManager().beginTransaction().replace(R.id.content_nav_menu,grafica).commit();
                }
            });
        }else {
            im.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }


        return view;
    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

}
