package consultores.detecsa.mau.gambusinodata.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Modelos.EgresosModel;
import consultores.detecsa.mau.gambusinodata.R;

/**
 * Created by MAU on 29/01/2018.
 */

public class EgresosAdapter extends RecyclerView.Adapter<EgresosAdapter.ViewHolderEgresos> {
    List<EgresosModel> listaEgresos;

    public EgresosAdapter(List<EgresosModel> listaEgresos) {
        this.listaEgresos = listaEgresos;
    }

    @Override
    public ViewHolderEgresos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.egresoslayout,null,false);
        return new ViewHolderEgresos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderEgresos holder, int position) {
        holder.llenarDatos(listaEgresos.get(position));
    }

    @Override
    public int getItemCount() {
        return listaEgresos.size();
    }

    public class ViewHolderEgresos extends RecyclerView.ViewHolder {
        TextView id,nombre,fecha,total,proveedor,costo,iva,fechacobro,folio;

        public ViewHolderEgresos(View itemView) {
            super(itemView);
            id=itemView.findViewById(R.id.tvIdEgresoRV);
            nombre=itemView.findViewById(R.id.tvEgresoConceptoRv);
            fecha=itemView.findViewById(R.id.tvFechEgresorv);
            proveedor=itemView.findViewById(R.id.tvClienteEgresoRv);
            total=itemView.findViewById(R.id.tvTotalEgresoRv);
            costo=itemView.findViewById(R.id.tvCostoEgresoRv);
            iva=itemView.findViewById(R.id.tvIvaEgresoRv);
            fechacobro=itemView.findViewById(R.id.tvFechCobroEgresoRv);
            folio=itemView.findViewById(R.id.tvFolioEgresoRv);
        }

        public void llenarDatos(EgresosModel egresosModel) {
            id.setText(egresosModel.getIdEgreso());
            nombre.setText(egresosModel.getEgreso());
            fecha.setText(egresosModel.getFecha());
            proveedor.setText(egresosModel.getProveedor());
            total.setText("$"+egresosModel.getTotal());
            folio.setText(egresosModel.getFolio());
            costo.setText(egresosModel.getCosto());
            iva.setText(egresosModel.getIVA());
            fechacobro.setText(egresosModel.getFechaPago());
        }
    }
}
