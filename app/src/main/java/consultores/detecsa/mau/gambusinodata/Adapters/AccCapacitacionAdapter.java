package consultores.detecsa.mau.gambusinodata.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.AccionesCapacitacionModel;
import consultores.detecsa.mau.gambusinodata.R;

/**
 * Created by MAU on 21/02/2018.
 */

public class AccCapacitacionAdapter extends RecyclerView.Adapter<AccCapacitacionAdapter.ViewHolder> {

    List<AccionesCapacitacionModel> lista;

    public AccCapacitacionAdapter(List<AccionesCapacitacionModel> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.accionesventalayout,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.llenarDatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView accion,anomes,meta,realizado,porc;

        public ViewHolder(View itemView) {
            super(itemView);
            accion=itemView.findViewById(R.id.tvAccionRvAccionVenta);
            anomes=itemView.findViewById(R.id.tvAñoFechaRvAccionVenta);
            meta=itemView.findViewById(R.id.tvMetaRvAccionVenta);
            realizado=itemView.findViewById(R.id.tvRealizadoRvAccionVenta);
            porc=itemView.findViewById(R.id.tvPorcentajeRvAccionVenta);
        }

        public void llenarDatos(AccionesCapacitacionModel model) {
            accion.setText(model.getAccionCapacitacionConcepto());
            anomes.setText(model.getAnoMes());
            meta.setText(model.getMeta());
            realizado.setText(model.getRealizado());
            Float real;
            if(model.getPorcAccion()=="" || model.getPorcAccion()==null){
                real=Float.parseFloat("0");
            }else {
                real= Float.parseFloat(model.getPorcAccion());
            }
            porc.setText(real.toString());
        }
    }
}
