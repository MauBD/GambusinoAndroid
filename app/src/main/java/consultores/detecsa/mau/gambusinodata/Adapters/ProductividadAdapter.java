package consultores.detecsa.mau.gambusinodata.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Modelos.ProductividadModel;
import consultores.detecsa.mau.gambusinodata.R;

import static android.R.id.list;

/**
 * Created by Desarrollo3 on 13/02/2018.
 */

public class ProductividadAdapter extends RecyclerView.Adapter<ProductividadAdapter.ViewHolderProductividad>{
    List<ProductividadModel> lista;

    public ProductividadAdapter(List<ProductividadModel> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolderProductividad onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.prodlayout,null,false);
        return new ViewHolderProductividad(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderProductividad holder, int position) {
        holder.llenarDatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }


    public class ViewHolderProductividad extends RecyclerView.ViewHolder {

        TextView idempleado,nombre,departamento,tipo;
        LinearLayout ll;
        public ViewHolderProductividad(View itemView) {
            super(itemView);
            idempleado=itemView.findViewById(R.id.tvNumeroProductividad);
            nombre=itemView.findViewById(R.id.tvEmpleadoProductividad);
            departamento=itemView.findViewById(R.id.tvDepartamentoProductividad);
            tipo=itemView.findViewById(R.id.tvTipoProductividadRv);
            ll=itemView.findViewById(R.id.layoutprod);
        }

        public void llenarDatos(ProductividadModel model){
            idempleado.setText(model.getIdEmpleado());
            nombre.setText(model.getEmpleado());
            departamento.setText(model.getDepartamento());
            tipo.setText(model.getTipoProductividad());
            ll.setMinimumWidth(20000);

        }
    }
}
