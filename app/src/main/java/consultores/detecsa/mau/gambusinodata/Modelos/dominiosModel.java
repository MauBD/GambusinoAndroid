
package consultores.detecsa.mau.gambusinodata.Modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dominiosModel {

    @SerializedName("Logotipo")
    @Expose
    private String logotipo;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("DominioAPP")
    @Expose
    private String dominioAPP;

    public String getLogotipo() {
        return logotipo;
    }

    public void setLogotipo(String logotipo) {
        this.logotipo = logotipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDominioAPP() {
        return dominioAPP;
    }

    public void setDominioAPP(String dominioAPP) {
        this.dominioAPP = dominioAPP;
    }

}
