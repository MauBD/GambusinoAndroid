package consultores.detecsa.mau.gambusinodata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccionesCapacitacionModel {
    @SerializedName("IdAccionCapacitacionConcepto")
    @Expose
    private String idAccionCapacitacionConcepto;

    @SerializedName("AccionCapacitacionConcepto")
    @Expose
    private String accionCapacitacionConcepto;

    @SerializedName("ano_mes")
    @Expose
    private String anoMes;

    @SerializedName("realizado")
    @Expose
    private String realizado;

    @SerializedName("meta")
    @Expose
    private String meta;

    @SerializedName("porc_accion")
    @Expose
    private String porcAccion;

    public String getIdAccionCapacitacionConcepto() {
        return idAccionCapacitacionConcepto;
    }

    public void setIdAccionCapacitacionConcepto(String idAccionCapacitacionConcepto) {
        this.idAccionCapacitacionConcepto = idAccionCapacitacionConcepto;
    }

    public String getAccionCapacitacionConcepto() {
        return accionCapacitacionConcepto;
    }

    public void setAccionCapacitacionConcepto(String accionCapacitacionConcepto) {
        this.accionCapacitacionConcepto = accionCapacitacionConcepto;
    }

    public String getAnoMes() {
        return anoMes;
    }

    public void setAnoMes(String anoMes) {
        this.anoMes = anoMes;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getPorcAccion() {
        return porcAccion;
    }

    public void setPorcAccion(String porcAccion) {
        this.porcAccion = porcAccion;
    }

}
