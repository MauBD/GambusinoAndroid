package consultores.detecsa.mau.gambusinodata.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Modelos.GastosModel;
import consultores.detecsa.mau.gambusinodata.R;

public class GastosFijosAdapter extends RecyclerView.Adapter<GastosFijosAdapter.ViewHolderGastosFijos>{
    List<GastosModel> listaGastos;

    public GastosFijosAdapter(List<GastosModel> listaGastos) {
        this.listaGastos = listaGastos;
    }

    @Override
    public ViewHolderGastosFijos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.gastosfijoslayout,null,true);
        return new ViewHolderGastosFijos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderGastosFijos holder, int position) {
        holder.llenarDatos(listaGastos.get(position));
    }

    @Override
    public int getItemCount() {
        return listaGastos.size();
    }

    public class ViewHolderGastosFijos extends RecyclerView.ViewHolder {
        TextView id,fecha,gasto,proveedor,total;

        public ViewHolderGastosFijos(View itemView) {
            super(itemView);
            id=itemView.findViewById(R.id.tvIdGastofijoRV);
            fecha=itemView.findViewById(R.id.tvFechagastofijorv);
            gasto=itemView.findViewById(R.id.tvGastoFijoRv);
            proveedor=itemView.findViewById(R.id.tvProveedorGastofijoRv);
            total=itemView.findViewById(R.id.tvTotalGastofijorv);
        }

        public void llenarDatos(GastosModel gastosModel) {
            id.setText(gastosModel.getIdGastoFijo());
            fecha.setText(gastosModel.getFecha());
            gasto.setText(gastosModel.getGastoFijoConcepto());
            proveedor.setText(gastosModel.getProveedor());
            total.setText("$"+gastosModel.getTotal());
        }
    }
}
