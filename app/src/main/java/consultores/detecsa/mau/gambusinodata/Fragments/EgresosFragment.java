package consultores.detecsa.mau.gambusinodata.Fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.Adapters.EgresosAdapter;
import consultores.detecsa.mau.gambusinodata.Fragments.DetallesFragments.IngresoDetalleFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.Graficas.GraficasFragment;
import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Modelos.EgresosModel;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EgresosFragment extends Fragment {

    TextView tv;
    ImageView im;
    private static ConnectivityManager manager;
    RecyclerView rv;
    EgresosAdapter adapter;
    FloatingActionButton verGrafica;
    View view;
    public EgresosFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_egresos, container, false);
        rv=view.findViewById(R.id.rvEgresos);
        verGrafica=view.findViewById(R.id.floatingChartEgresos);
        tv= view.findViewById(R.id.tvNetworkErrorEgresos);
        im=view.findViewById(R.id.imageViewNetworkEgresos);

        if (isOnline(getActivity())) {
            rv.setVisibility(View.VISIBLE);
            verGrafica.setVisibility(View.VISIBLE);

            //String baseURL= "http://gambusinodata.detecsa-consultores.com/";
            Datos d = new Datos(getContext());
            String baseURL= d.getDominio();
            Retrofit retrofit=new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();
            WebService webservice= retrofit.create(WebService.class);
            Call<List<EgresosModel>> egresos=webservice.egresos();
            egresos.enqueue(new Callback<List<EgresosModel>>() {
                @Override
                public void onResponse(Call<List<EgresosModel>> call, Response<List<EgresosModel>> response) {
                    if(response.body().size()<1){
                        rv.setVisibility(View.INVISIBLE);
                        im.setVisibility(View.VISIBLE);
                        tv.setVisibility(View.VISIBLE);
                        im.setImageDrawable(getResources().getDrawable(R.drawable.ic_alert));
                        tv.setText("¡No hay elementos que mostrar!");
                    }else {
                        adapter = new EgresosAdapter(response.body());
                        rv.setAdapter(adapter);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                }

                @Override
                public void onFailure(Call<List<EgresosModel>> call, Throwable t) {

                }
            });

            verGrafica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment grafica=new GraficasFragment();
                    Bundle bundle = new Bundle();
                    Datos d = new Datos(getContext());
                    String baseURL2= d.getDominio();
                    bundle.putString("url",baseURL2+"webServiceApp/grafica_egresos");
                    grafica.setArguments(bundle);

                    getFragmentManager().beginTransaction().replace(R.id.content_nav_menu,grafica).commit();
                }
            });

            final GestureDetector mGestureDetector = new GestureDetector(view.getContext(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });

            rv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean b) {

                }

                @Override
                public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                    try {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                            int position = recyclerView.getChildAdapterPosition(child);

                            String id = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvIdEgresoRV)).getText().toString();
                            String folio = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvFolioEgresoRv)).getText().toString();
                            String fecha = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvFechEgresorv)).getText().toString();
                            String concepto = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvEgresoConceptoRv)).getText().toString();
                            String provedor = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvClienteEgresoRv)).getText().toString();
                            String costo = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvCostoEgresoRv)).getText().toString();
                            String iva = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvIvaEgresoRv)).getText().toString();
                            String total = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvTotalEgresoRv)).getText().toString();
                            String fechacobro = ((TextView) recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tvFechCobroEgresoRv)).getText().toString();

                            Fragment detalles= new IngresoDetalleFragment();
                            Bundle bundle= new Bundle();
                            bundle.putString("tipo","egreso");
                            bundle.putString("id",id);
                            bundle.putString("folio",folio);
                            bundle.putString("fecha",fecha);
                            bundle.putString("concepto",concepto);
                            bundle.putString("proveedor",provedor);
                            bundle.putString("costo",costo);
                            bundle.putString("iva",iva);
                            bundle.putString("total",total);
                            bundle.putString("fechapago",fechacobro);
                            detalles.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.content_nav_menu,detalles).commit();

                            return true;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                }
            });
        }else {
            im.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }



        return view;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }


}
