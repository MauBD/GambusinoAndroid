package consultores.detecsa.mau.gambusinodata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class preferenciasClientesModel {

    @SerializedName("IdPreferenciaClienteConcepto")
    @Expose
    private String idPreferenciaClienteConcepto;
    @SerializedName("PreferenciaClienteConcepto")
    @Expose
    private String preferenciaClienteConcepto;
    @SerializedName("ano_mes")
    @Expose
    private String anoMes;
    @SerializedName("realizado")
    @Expose
    private String realizado;
    @SerializedName("meta")
    @Expose
    private String meta;
    @SerializedName("porc_accion")
    @Expose
    private String porcAccion;

    public String getIdPreferenciaClienteConcepto() {
        return idPreferenciaClienteConcepto;
    }

    public void setIdPreferenciaClienteConcepto(String idPreferenciaClienteConcepto) {
        this.idPreferenciaClienteConcepto = idPreferenciaClienteConcepto;
    }

    public String getPreferenciaClienteConcepto() {
        return preferenciaClienteConcepto;
    }

    public void setPreferenciaClienteConcepto(String preferenciaClienteConcepto) {
        this.preferenciaClienteConcepto = preferenciaClienteConcepto;
    }

    public String getAnoMes() {
        return anoMes;
    }

    public void setAnoMes(String anoMes) {
        this.anoMes = anoMes;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getPorcAccion() {
        return porcAccion;
    }

    public void setPorcAccion(String porcAccion) {
        this.porcAccion = porcAccion;
    }

}