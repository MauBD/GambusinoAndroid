package consultores.detecsa.mau.gambusinodata.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Fragments.AccCapacitacionFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.AccionesVenteFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.CalidadFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.DashboardFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.EgresosFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.GastosFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.IngresosFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.PreferenciasFragment;
import consultores.detecsa.mau.gambusinodata.Fragments.productividadFragment;
import consultores.detecsa.mau.gambusinodata.R;

public class NavMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView labelnombre,labelmail;
    Fragment fragment;
    Datos datos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_menu);
        datos= new Datos(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //cambiando el nombre de usuario en el menu
        View headerView = navigationView.getHeaderView(0);
        labelnombre = (TextView) headerView.findViewById(R.id.labelusuario);
        labelnombre.setText(getIntent().getStringExtra("user"));
        labelmail = (TextView) headerView.findViewById(R.id.labelmail);
        labelmail.setText(getIntent().getStringExtra("mail"));
        // establecer el dashboard como fragment principal
        fragment = new DashboardFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
        getSupportActionBar().setTitle("Dashboard");

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salir) {
            finish();
            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            datos.setUsr("");
            datos.setPwd("");
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        fragment= null;
//        boolean selected=false;

        if (id == R.id.nav_ingresos) {
            fragment= new IngresosFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Ingresos");

        } else if (id == R.id.nav_egresos) {
            fragment= new EgresosFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Egresos");

        } else if (id == R.id.nav_gastos) {
            fragment=new GastosFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Gastos Fijos");

        } else if (id == R.id.nav_product) {
            fragment=new productividadFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Productividad");
        } else if (id == R.id.acciones_venta) {
            fragment = new AccionesVenteFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Acciones de venta");

        } else if (id == R.id.nav_calidad) {
            fragment= new CalidadFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Calidad de servicio");
        } else if (id == R.id.nav_capacitacion) {
            fragment= new AccCapacitacionFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Acciones de capacitación");
        } else if (id == R.id.nav_preferencias) {
            fragment= new PreferenciasFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_nav_menu,fragment).commit();
            getSupportActionBar().setTitle("Preferencias del cliente");
        }




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
