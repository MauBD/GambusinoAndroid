package consultores.detecsa.mau.gambusinodata.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.preferenciasClientesModel;

/**
 * Created by Desarrollo3 on 20/02/2018.
 */

public class PreferenciasAdapter extends RecyclerView.Adapter<PreferenciasAdapter.ViewHolder> {

    List<preferenciasClientesModel> lista;

    public PreferenciasAdapter(List<preferenciasClientesModel> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.accionesventalayout,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.llenarDatos(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView accion,anomes,meta,realizado,porcentaje;
        public ViewHolder(View itemView) {
            super(itemView);
            accion= (TextView) itemView.findViewById(R.id.tvAccionRvAccionVenta);
            anomes=(TextView) itemView.findViewById(R.id.tvAñoFechaRvAccionVenta);
            meta=(TextView) itemView.findViewById(R.id.tvMetaRvAccionVenta);
            realizado=itemView.findViewById(R.id.tvRealizadoRvAccionVenta);
            porcentaje=(TextView) itemView.findViewById(R.id.tvPorcentajeRvAccionVenta);
        }

        public void llenarDatos(preferenciasClientesModel model) {
            accion.setText(model.getPreferenciaClienteConcepto());
            anomes.setText(model.getAnoMes());
            meta.setText(model.getMeta());
            realizado.setText(model.getRealizado());
            Float Realizado=(Float.parseFloat(model.getPorcAccion())*100);
            porcentaje.setText(Realizado.toString());
        }
    }
}
