package consultores.detecsa.mau.gambusinodata.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import consultores.detecsa.mau.gambusinodata.Modelos.Datos;
import consultores.detecsa.mau.gambusinodata.Modelos.Usuariomodel;
import consultores.detecsa.mau.gambusinodata.R;
import consultores.detecsa.mau.gambusinodata.WebService;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity implements View.OnClickListener {

    TextView txtuser,txtpwd;
    Button btn;
    ImageButton settings;
    Usuariomodel usr;
    Datos datos;
    String mailusr,pwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        datos=new Datos(getApplicationContext());
        txtuser=(TextView) findViewById(R.id.txtUser);
        txtpwd=(TextView) findViewById(R.id.txtPwd);
        btn=(Button) findViewById(R.id.btn_login);
        settings= (ImageButton) findViewById(R.id.imgBSettings);

        btn.setOnClickListener(this);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent set= new Intent(Login.this,ConfiguracionActivity.class);
                startActivity(set);
                //finish();
            }
        });

    }

    @Override
    public void onClick(View view) {
        final View v=view;
        if (validar()){
            if(datos.getDominio()!=null){
                final ProgressDialog progressDialog = new ProgressDialog(Login.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Autenticando...");
                progressDialog.show();

                //String baseURL= "http://gambusinodata.detecsa-consultores.com/";
                String baseURL= datos.getDominio();
                Retrofit retrofit=new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();

                WebService webservice= retrofit.create(WebService.class);
                mailusr=txtuser.getText().toString();
                pwd=txtpwd.getText().toString();
                retrofit2.Call<Usuariomodel> usuario=webservice.login(mailusr,pwd);
                usuario.enqueue(new Callback<Usuariomodel>() {
                    @Override
                    public void onResponse(retrofit2.Call<Usuariomodel> call, Response<Usuariomodel> response) {
                        if(response.isSuccessful()){
                            usr= response.body();
                            if(usr.getSuccess()){
                                datos.setUsr(mailusr);
                                datos.setPwd(pwd);
                                Intent menu= new Intent(Login.this,NavMenu.class);
                                menu.putExtra("user",usr.getNombreusuario());
                                menu.putExtra("mail",usr.getCorreo());
                                //progressDialog.dismiss();
                                startActivity(menu);
                                finish();
                            }else {
                                progressDialog.dismiss();
                                Snackbar.make(v , "usuario y/o contraseña incorrecta", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }else {
                            progressDialog.dismiss();
                            Snackbar.make(v , "Comprueba tu conexion a intenet o intentalo mas tarde!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }
                    @Override
                    public void onFailure(retrofit2.Call<Usuariomodel> call, Throwable t) {
                        progressDialog.dismiss();
                        Snackbar.make(v , "Comprueba tu conexion a intenet o intentalo mas tarde!", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });
            }else {
                Toast.makeText(this, "Debe ingresar la clave de su empresa!", Toast.LENGTH_SHORT).show();
                Intent set= new Intent(Login.this,ConfiguracionActivity.class);
                startActivity(set);
            }
        }


    }
    boolean validar(){
        boolean valid = true;

        String email = txtuser.getText().toString();
        String password = txtpwd.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtuser.setError("Ingresa un email valido!");
            valid = false;
        } else {
            txtuser.setError(null);
        }

        if (password.isEmpty()) {
            txtpwd.setError("Ingresa una contraseña!");
            valid = false;
        } else {
            txtpwd.setError(null);
        }

        return valid;
    }
    int Autenticacion(){
        final int[] result = new int[1];
        return result[0];
    }
}

